/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translatorxxx;

import static java.awt.SystemColor.window;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author conve
 */
public class NewFile extends Application {

    Stage window;
    Scene scene1;
    String language1;
    String language2;
    JSONArray jsonArray1 = new JSONArray();
    JSONArray jsonArray2 = new JSONArray();
    JSONArray words1 = new JSONArray();
    JSONArray words2 = new JSONArray();
    ArrayList<TextField> taal1 = new ArrayList<TextField>();
    ArrayList<TextField> taal2 = new ArrayList<TextField>();
    TextField taalField = new TextField("");
    TextField taalField2 = new TextField("");
    Menu itmTest = new Menu("Test");
    Button addButton = new Button("+");
    GridPane grid = new GridPane();
    
    public void openAFile(){
            //Setting up the file Chooser
            FileChooser fileChooser = new FileChooser();
            
            //Adding extentions
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Extentions", "*.json");
            fileChooser.getExtensionFilters().add(extFilter);

            fileChooser.setTitle("Open JSON");
            File open = fileChooser.showOpenDialog(window);

            if (open != null) {
                BufferedReader bufferedReader = null;
                try {
                    bufferedReader = new BufferedReader(new FileReader(open));
                    JSONObject jsonFile = new JSONObject(bufferedReader.readLine());
                    
                    //Geting data from the file                
                    this.language1 = jsonFile.get("lang1").toString();
                    this.language2 = jsonFile.get("lang2").toString();
                    this.words1 = jsonFile.getJSONArray("woorden1");
                    this.words2 = jsonFile.getJSONArray("woorden2");
                    
                    
                    //Enabling test
                    this.setTest();                     
                } catch (Exception exception) {}
                
                //Showing all the words on the screen                
                taalField.setText(language1);
                taalField2.setText(language2);
                
                for (int i = 0; i <= words1.length(); i++) {                    
                    if (i == 0) {
                        try {                            
                            taal1.get(i).setText("" + words1.getString(i));
                            taal2.get(i).setText("" + words2.getString(i));
                        } catch (Exception edawException) {}

                                } else {
                                         try {
                                             //Make all the words for the first language
                                              TextField woord = new TextField("" + words1.getString(i));
                                              woord.setPromptText("Woord van de eerste taal");
                                              GridPane.setConstraints(woord, 0, taal1.size() + 3);
                                              taal1.add(woord);

                                              //Make next TextField for the next language
                                              TextField woordVandeTweedeTaal = new TextField("" + words2.getString(i));
                                              woordVandeTweedeTaal.setPromptText("Woord van de tweede taal");
                                              GridPane.setConstraints(woordVandeTweedeTaal, 2, taal2.size() + 3);
                                              taal2.add(woordVandeTweedeTaal);

                                              //Set new places
                                              grid.getChildren().remove(addButton);
                                              GridPane.setConstraints(addButton, 3, taal1.size() + 2);
                                              grid.getChildren().addAll(addButton, woord, woordVandeTweedeTaal);
                        } catch (Exception dawExceptione) {}
                    }
                }
            } 
        }
    
    //Anabling the test button
    public void setTest() {        
                // Making the menu Test
                MenuItem test = new MenuItem("Test from " + language1 + " to " + language2);
                MenuItem test2 = new MenuItem("Test from " + language2 + " to " + language1);
                //Adding action to the test menu
                test.setOnAction(a -> {
                    Test toets = new Test(words1, words2, language1, language2);
                    toets.start(window);
                });
                test2.setOnAction(a -> {
                    Test toets = new Test(words2, words1, language2, language1);
                    toets.start(window);
                });
                itmTest.getItems().addAll(test, test2); 
    }

    @Override
    public void start(Stage primaryStage) {
        
        // Setting up the window
        window = primaryStage;
        window.setTitle("TranslatorXXX new file");

        //settig up the menubar
        MenuBar menubar = new MenuBar();
        Menu file = new Menu("File");
        
        //Setting ip the New Menu and adding the action to this
        MenuItem itmNew = new MenuItem("New...");
        NewFile newFile = new NewFile();
        itmNew.setOnAction(e -> newFile.start(primaryStage));
        
        MenuItem itmSave = new MenuItem("Save");
       
        //Setting up the grid panel
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setPercentWidth(45);
        ColumnConstraints column2 = new ColumnConstraints();
        column2.setPercentWidth(2);
        ColumnConstraints column3 = new ColumnConstraints();
        column3.setPercentWidth(45);
        ColumnConstraints column4 = new ColumnConstraints();
        column4.setPercentWidth(8);

        grid.getColumnConstraints().addAll(column1, column2, column3, column4);
    
        //Adding the SaveAs menu and ading the action to this        
        MenuItem itmSaveAs = new MenuItem("Save As...");
        
        itmSaveAs.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            
            //Adding ExtentionFilter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON files", ".json");
            fileChooser.getExtensionFilters().add(extFilter);

            fileChooser.setTitle("Save JSON");
            
            //Show the save window
            File save = fileChooser.showSaveDialog(primaryStage);
            
            if (save != null) {
                try {
                    FileWriter fileWriter = new FileWriter(save);
                    
                    //Putting the json file together
                    JSONObject jsonFile = new JSONObject();
                    try {
                        jsonFile.put("lang1", language1);
                        jsonFile.put("lang2", language2);
                        for (TextField field : taal1) {
                            jsonArray1.put(field.getText());
                        }
                        for (TextField field : taal2) {
                            jsonArray2.put(field.getText());
                        }
                        jsonFile.put("woorden1", jsonArray1);
                        jsonFile.put("woorden2", jsonArray2);
                        words1 = jsonArray1;
                        words2 = jsonArray2;
                    } catch (Exception eqException) {
                    }
                    
                    //Actual writing to a file
                    fileWriter.write(jsonFile.toString());
                    
                    //closing the save window
                    fileWriter.close();
                } catch (IOException ex) {                    
                }
            }
        });
        
        // open menu
        MenuItem itmOpen = new MenuItem("Open...");
        
        //method to open a file
        itmOpen.setOnAction(e -> this.openAFile());
        
        //Adding all the menus to the window
        menubar.getMenus().addAll(file);
        file.getItems().addAll(itmNew, itmSave, itmSaveAs, itmOpen, itmTest);

        //Adding elements to the grid
        Label lable1 = new Label("Taal 1");
        GridPane.setConstraints(lable1, 0, 0);

        //Adding the button + to the window
        GridPane.setConstraints(addButton, 3, 3);
        
        //Setting up the grid for the starting view
        taalField.setPromptText("Taal 1");
        GridPane.setConstraints(taalField, 2, 0);

        Label lable2 = new Label("Taal 2");
        GridPane.setConstraints(lable2, 0, 1);

        taalField2.setPromptText("Taal 2");
        GridPane.setConstraints(taalField2, 2, 1);

        Label lable3 = new Label("Woorden van Taal 1");
        GridPane.setConstraints(lable3, 0, 2);

        TextField woord1 = new TextField("");
        woord1.setPromptText("Woord van de eerste taal");
        GridPane.setConstraints(woord1, 0, 3);

        Label lable4 = new Label("Woord van Taal 2");
        GridPane.setConstraints(lable4, 2, 2);

        TextField woord2 = new TextField("");
        woord2.setPromptText("Woorden van de tweede taal");
        GridPane.setConstraints(woord2, 2, 3);
        
        //Adding the first 2 textFields to the array
        taal1.add(woord1);
        taal2.add(woord2);

        grid.getChildren().addAll(lable1, taalField, lable2, taalField2, lable3, woord1, lable4, woord2, addButton);
        
        //Adding the scrollbalk
        ScrollPane scrollPane = new ScrollPane();
        grid.setMinWidth(600);
        

        scrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);

        //BorderPane setup
        BorderPane bp = new BorderPane();
        bp.setTop(menubar);
        bp.setCenter(grid);
        scrollPane.setContent(bp);

        
        //Adding functionality to the button
        addButton.setOnAction(e -> {
          
            language1 = taalField.getText();
            language2 = taalField2.getText();

            //Maken next TextField for the first language
            TextField woord = new TextField("");
            woord.setPromptText("Woord van de eerste taal");
            GridPane.setConstraints(woord, 0, taal1.size() + 3);
            taal1.add(woord);

            //Make next TextField for the next language
            TextField woordVandeTweedeTaal = new TextField("");
            woordVandeTweedeTaal.setPromptText("Woord van de tweede taal");
            GridPane.setConstraints(woordVandeTweedeTaal, 2, taal2.size() + 3);
            taal2.add(woordVandeTweedeTaal);

            //Set new places
            grid.getChildren().remove(addButton);
            GridPane.setConstraints(addButton, 3, taal1.size() + 2);
            grid.getChildren().addAll(addButton, woord, woordVandeTweedeTaal);
        });

        //Settings for Scene       
        scene1 = new Scene(scrollPane, 700, 600);

        //Adding scene to the window
        window.setScene(scene1);

        //Executing the window
        window.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}
