/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translatorxxx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
/**
 *
 * @author conve
 */
public class TranslatorXXX extends Application {
    
    Stage window;
    Scene scene1;
    
    @Override
    public void start(Stage primaryStage) {   
       // Setting up the window
       window = primaryStage;
       window.setTitle("TranslatorXXX");
       
       //settig up the menubar
       MenuBar menubar = new MenuBar();
       Menu file = new Menu("File");
       
       //setting up the New File menu and adding the action for this
       MenuItem itmNew = new MenuItem("New...");
       NewFile newFile = new NewFile();
       itmNew.setOnAction(e -> newFile.start(primaryStage));
       
       //setting up the Open File menu and adding the action for this   
       MenuItem itmOpen = new MenuItem("Open...");
       itmOpen.setOnAction(o -> {
           newFile.start(primaryStage);
           newFile.openAFile();
       });
       
       //adding all the menu's to the menubar
       menubar.getMenus().addAll(file);
       file.getItems().addAll(itmNew, itmOpen);
       
       //Setting up the center word "Welcome!"
       StackPane layout = new StackPane();
       Label lable = new Label("Welcome!");
       lable.setTextAlignment(TextAlignment.CENTER);
       layout.getChildren().add(lable);
      
       //BorderPane setup
       BorderPane borderPane = new BorderPane();
       borderPane.setTop(menubar);
       borderPane.setCenter(layout);
       
       //Settings for Scene       
       scene1 = new Scene(borderPane, 500, 600);
       
       //Adding scene to the window
       window.setScene(scene1);     
       
       //Executing the window
       window.show();
    }

   
    public static void main(String[] args) {
        launch(args);
    }
    
}
