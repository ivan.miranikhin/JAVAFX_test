/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package translatorxxx;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import org.json.JSONArray;

/**
 *
 * @author conve
 */
public class Test extends Application{
    Stage window;
    Scene scene1;
    int aantalFouten = 0;
    
    String language1;
    String language2;
    
    JSONArray words1 = new JSONArray();
    JSONArray words2 = new JSONArray();
    int number;
    Label correctAns = new Label();
    
    Label status = new Label(); 
    
    TextField woordVanDeEerste = new TextField();
    TextField woordVanDeTweede = new TextField();
   
    
    public Test (JSONArray words1, JSONArray words2, String language1, String language2 ){
   this.words1 = words1;
   this.words2 = words2;
   this.language1 = language1;
   this.language2 = language2;
    }
     @Override
    public void start(Stage primaryStage) {
       //Setting up the window
       window = primaryStage;
       window.setTitle("TranslatorXXX"); 
       
       //Adding grid       
        GridPane grid = new GridPane();        
        grid.setVgap(10);
           
          //Setting GridLayout
          ColumnConstraints column1 = new ColumnConstraints();
          column1.setPercentWidth(50);
          ColumnConstraints column2 = new ColumnConstraints();
          column2.setPercentWidth(50);  
          grid.getColumnConstraints().addAll(column1, column2);
          
          //Adding Lables          
          Label naamVande1taal = new Label("" + language1);
          naamVande1taal.setFont(Font.font("Helvetica", 13.5));
          Label naamVande2taal = new Label("" + language2);
          naamVande2taal.setFont(Font.font("Helvetica", 13.5));
          
          //Adding all to the grid
          GridPane.setConstraints(naamVande1taal, 0, 0);          
          GridPane.setConstraints(naamVande2taal, 1, 0);
          grid.getChildren().addAll(naamVande1taal,naamVande2taal);
          
         //Setting the first textfield
         woordVanDeEerste.setPromptText("Put your answer here");
         
         //Enable action on pressing the "ENTER" key
         woordVanDeEerste.setOnKeyPressed(new EventHandler<KeyEvent>(){
             public void handle(KeyEvent e)
             {   //If Enter is pressed               
                 if (e.getCode()== KeyCode.ENTER) {
                     try{
                         //Checking the answer
                         //If correct
                     if (woordVanDeEerste.getText().equals(words1.getString(number))) {
                         status.setText("Correct");
                         correctAns.setText(words1.getString(number));
                         words2.remove(number);
                         words1.remove(number);
                         //If it was the last word
                         if (words2.length()!=0) {
                         number = (int)(Math.random()*words2.length());                          
                         woordVanDeEerste.setText("");
                         woordVanDeTweede.setText(words2.getString(number));                         
                         } 
                         else{
                             status.setText("Finished");
                             correctAns.setText("Number of mistakes " + aantalFouten);
                         }
                     }   //If it was not correct
                     else{
                         status.setText("Fout");
                         correctAns.setText(words1.getString(number));
                         aantalFouten++;
                         
                         number = (int)(Math.random()*words2.length());
                         woordVanDeEerste.setText("");
                         woordVanDeTweede.setText(words2.getString(number));                         
                     }
                     }
                     catch (Exception aException){}                    
                 }
                 else{
                     status.setText("");
                     correctAns.setText("");
                 }
             }
         });
         //The next word should be a random one
         number =(int) (Math.random()*words2.length());
         try {
             woordVanDeTweede.setText(words2.getString(number));
         } catch (Exception e) {
         }         
         //Setting grid layout
         GridPane.setConstraints(woordVanDeEerste, 0, 1);          
         GridPane.setConstraints(woordVanDeTweede, 1, 1);
         grid.getChildren().addAll(woordVanDeEerste, woordVanDeTweede);
         
                         
         status.setFont(Font.font("Helvetica", FontPosture.ITALIC, 18.0));         
         status.setTextAlignment(TextAlignment.CENTER); 
         GridPane.setConstraints(status, 0, 2);
         grid.getChildren().addAll(status);
         
         
         correctAns.setFont(Font.font("Helvetica", 18.0));
         GridPane.setConstraints(correctAns, 1, 2);
         grid.getChildren().addAll(correctAns);
         
          
          //settig up the menubar
       MenuBar menubar = new MenuBar();
       Menu file = new Menu("File");
       MenuItem itmNew = new MenuItem("New...");
       NewFile newFile = new NewFile();
       itmNew.setOnAction(e -> newFile.start(primaryStage));
       MenuItem itmOpen = new MenuItem("Open...");
       itmOpen.setOnAction(e -> {
           newFile.start(primaryStage);
           newFile.openAFile();
       });
       menubar.getMenus().addAll(file);
       file.getItems().addAll(itmNew, itmOpen);
       
       //Setting up the center word "Welcome!"
       StackPane layout = new StackPane();
      
      
       //BorderPane setup
       BorderPane borderPane = new BorderPane();
       borderPane.setTop(menubar);
       borderPane.setCenter(grid);
       
       
       
       //Settings for Scene       
       scene1 = new Scene(borderPane, 500, 600);
       
       //Adding scene to the window
       window.setScene(scene1);     
       
       //Executing the window
       window.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
